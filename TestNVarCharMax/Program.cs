﻿using System.Data.SqlClient;
using System.Data;
using System;

namespace TestNVarCharMax {
	class MainClass {
		static string ConnectionString =
			@"data source=localhost;initial catalog=TestDB;User ID=DebugUser;Password=monotest;persist security info=False;packet size=4096;Connection Timeout=300";

		public static void Main() {

			const string commandText = @"INSERT INTO NVarCharTest (LongName) VALUES (@LongName)";
			const int maxNVarCharLengthDotNet = 2147483647;
			const int maxNVarCharLengthMono = 1073741823;

			using (var connection = new SqlConnection(ConnectionString)) {
				connection.Open();
				using (var command = new SqlCommand(commandText, connection)) {
					var longNameParameter = new SqlParameter("@LongName", SqlDbType.NVarChar, maxNVarCharLengthDotNet);
					longNameParameter.Value = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.";

					command.Parameters.Add(longNameParameter);

					try {
						command.ExecuteNonQuery();
					}
					catch (Exception ex) {
						Console.WriteLine(ex.Message);
						Console.WriteLine(ex.StackTrace);
					}

					longNameParameter.Size = maxNVarCharLengthMono;

					try {
						command.ExecuteNonQuery();
					}
					catch (Exception ex) {
						Console.WriteLine(ex.Message);
						Console.WriteLine(ex.StackTrace);
					}
				}
			}
		}
	}
}
